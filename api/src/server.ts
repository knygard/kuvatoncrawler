import express from 'express';
import clc from 'cli-color';
import firebaseAdmin from 'firebase-admin';
import { QueryDocumentSnapshot } from 'firebase-admin/firestore';

const serviceAccount = require('../kuvatonbrowser-firebase-adminsdk-849q5-262ab22dc3.json');

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
});

const firestoreDb = firebaseAdmin.firestore();
const kuvatonCollection = firestoreDb.collection('kuvatondb');
const documentIdField = firebaseAdmin.firestore.FieldPath.documentId();

const port = process.env.PORT || 8090;

const app = express();

if (process.env.NODE_ENV !== 'production') {
  app.set('json spaces', 2);
}

const router = express.Router();

app.get('/', (_, res) => res.send('OK'));

router.use(function (_, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

const docToKuvatonObj = (doc: QueryDocumentSnapshot) => ({
  id: doc.id,
  imageId: doc.get('imageId'),
  imageUrl: doc.get('imageUrl'),
  thumbnailUrl: doc.get('thumbnailUrl'),
  browserUrl: doc.get('browserUrl'),
  title: doc.get('title'),
  rating: doc.get('rating'),
  comments: doc.get('comments'),
});

router.get('/images', async (req, res) => {
  const { limit: queryLimit, direction = 'next' } = req.query;

  const imageId =
    req.query.imageId && typeof req.query.imageId === 'string'
      ? parseInt(req.query.imageId)
      : undefined;

  if (imageId && !Number.isFinite(imageId)) {
    return res.status(400).send('Invalid image id');
  }

  const queryLimitNumber =
    typeof queryLimit === 'string' && Number.isInteger(parseInt(queryLimit))
      ? parseInt(queryLimit)
      : undefined;

  const limitParam = queryLimitNumber && queryLimitNumber <= 100 ? queryLimitNumber : 50;

  const querySnapshot = imageId
    ? await kuvatonCollection
        .where(['imageId', direction === 'previous' ? '>' : '<', imageId])
        .orderBy('imageId', 'desc')
        .limitToLast(limitParam)
        .get()
    : await kuvatonCollection.limit(limitParam).orderBy('imageId', 'desc').get();

  return res.json(querySnapshot.docs.map(docToKuvatonObj));
});

router.get('/images/:imageId', async (req, res) => {
  const imageId = parseInt(req.params.imageId);

  if (!Number.isFinite(imageId)) {
    return res.status(400).send('Invalid image id');
  }

  const querySnapshot = await kuvatonCollection
    .where('imageId', '==', imageId)
    .orderBy(documentIdField)
    .get();

  if (querySnapshot.docs.length === 0) {
    return res.sendStatus(404);
  }

  return res.json(docToKuvatonObj(querySnapshot.docs[0]));
});

app.use('/v1', router);

app.listen(port);

console.log(clc.green('kuvatonCrawler server listening on port ' + port));
