# KuvatonBrowser Cloud Functions (Firebase)

## Google Cloud functions

Region: `europe-west3`

https://cloud.google.com/functions/docs/running/direct

## Usage

```sh
npm install
```

```sh
npm install -g firebase-tools
```

Run `firebase login`` to log in via the browser and authenticate the Firebase CLI.

Maybe needed for `gcloud`:

```sh
gcloud init
gcloud auth login
gcloud config set project kuvatonbrowser
```

Test locally:

```sh
cd cloud-functions && npm run build && cd ../
firebase emulators:start --project kuvatonbrowser
open http://127.0.0.1:5001/kuvatonbrowser/europe-north1/getImages
```

Remember to build manually! The emulator will only read from `lib/`.

Deploy:

```sh
firebase deploy --only functions
```

To debug:

```sh
firebase deploy --only functions --debug
```

### Crawler function

https://cloud.google.com/functions/docs/running/direct

Not sure if works:

```sh
gcloud functions call crawlImages --data '{"maxPages":1}'
```

Should work:

```sh
curl -X POST -H "Content-Type: application/json" -d '{"data":{"maxPages":1}}' http://127.0.0.1:5001/kuvatonbrowser/europe-north1/crawlImages
```

Should not work because is not public:

```sh
curl -X POST -H "Content-Type: application/json" -d '{"data":{"maxPages":1}}' https://europe-north1-kuvatonbrowser.cloudfunctions.net/crawlImages
```

### Misc Notes

Cloud Functions 2nd gen use "Cloud Run" instead of "Cloud Functions"
https://cloud.google.com/functions/docs/concepts/version-comparison

The message from Pub/Sub need to be unwrapped
https://cloud.google.com/pubsub/docs/payload-unwrapping
It's in different format than the data payload expected by the function endpoint (`{"data": {...}}`).
See esp. https://cloud.google.com/pubsub/docs/payload-unwrapping#configure_payload_unwrapping

---

Couldn't get authenticated pub/sub requests to work. It always complained 401 not authorized, even though it was using the firebasesdk admin service token with all correct rights. I suspect the auth header was not correctly formed.

I ended up making it "internal network traffic only" (at Cloud Run / instance / Networking) and disabling authentication.
