import * as path from 'node:path';
import * as url from 'node:url';
import * as Crawler from 'crawler';
// https://stackoverflow.com/questions/69746672/unable-to-resolve-path-to-module-firebase-admin-app-eslint
/* eslint-disable import/no-unresolved */
import { getFirestore } from 'firebase-admin/firestore';
/* eslint-enable import/no-unresolved */

import * as utils from './utils';

const settings = {
  entryUri: 'http://kuvaton.com',
  timeout: 5, // seconds
};

const firestoreDb = getFirestore();
const kuvatonCollection = firestoreDb.collection('kuvatondb');

const cache: { pageURIs: string[]; imageURIs: string[] } = { pageURIs: [], imageURIs: [] };

export interface Options {
  /**
   * Max. amount of pages to crawl
   */
  maxPages?: number;
  /**
   * Max. amount of images to crawl
   */
  maxImages?: number;
  /**
   * Crawl images until an existing image is found and update
   * given approximate amount of images after that
   */
  onlyNew?: boolean;
}

const findImageById = async (imageId: number) => {
  const querySnapshot = await kuvatonCollection.where('imageId', '==', imageId).get();
  return querySnapshot.docs[0] ?? undefined;
};

interface ImageEntry {
  imageId: number;
  imageUrl: string;
  thumbnailUrl: string;
  browseUrl: string;
  title: string;
  rating: number;
  comments: {
    date: Date;
    user: string;
    message: string;
  }[];
}

async function saveImageEntry(entry: ImageEntry) {
  const image = await findImageById(entry.imageId);

  if (image?.ref) {
    image.ref.update(entry as unknown as Record<string, string | number | object[]>);
  } else {
    await kuvatonCollection.doc().set(entry);
  }
}

let existingImageIDsCrawled = 0;

const individualImagePageCrawler = new Crawler({
  maxConnections: 10,
  jQuery: true,
  retries: 0,
  skipDuplicates: true,
  callback: async (error, result, done) => {
    const {
      $,
      options: { uri: browseUri = '' },
    } = result;

    if (error) {
      done();
      return console.error(error);
    }

    const imgSrc = $('#nayta_nykyinen img').attr('src');

    if (!imgSrc) {
      done();
      return;
    }

    const imageId = utils.captureKuvatonIdFromUri(browseUri);

    if (!imageId) {
      done();
      throw new Error(`imageId was not readable from uri: ${browseUri}`);
    }

    const imageEntry: ImageEntry = {
      imageId,
      imageUrl: url.parse(imgSrc).href,
      thumbnailUrl: url.parse(imgSrc.replace('/kuvei/', '/kuvei_thumb/')).href,
      browseUrl: url.parse(browseUri).href,
      title: $('#nayta_nykyinen .kuvanimi').text().trim() || path.basename(imgSrc),
      rating: parseInt($('#nayta_kuva .arvosana').text()),
      comments: $('#content .kuvakommentit table')
        .map((_, element) => {
          const comment = $(element);

          return {
            date: utils.parseKuvatonDate(comment.find('.aika').text()),
            user: utils.parseKuvatonUser(comment.find('.tunnus').text()),
            message: comment.find('.viesti').html() ?? '',
          };
        })
        .get(),
    };

    console.log(`Saving ${imageEntry.imageUrl}`);

    saveImageEntry(imageEntry);

    cache.imageURIs.push(imgSrc);

    done();
  },
});

export async function startCrawling(options: Options) {
  function shouldContinueFetching(options: Options) {
    if (options.maxImages && cache.imageURIs.length >= options.maxImages) {
      return false;
    }

    if (options.maxPages && cache.pageURIs.length >= options.maxPages) {
      return false;
    }

    if (options.onlyNew && existingImageIDsCrawled >= 20) {
      return false;
    }

    return true;
  }

  const pageCrawler = new Crawler({
    maxConnections: 1,
    jQuery: true,
    retries: 0,
    skipDuplicates: true,
    callback: async (error, result, done) => {
      const { $ } = result;

      if (error) {
        done();
        return console.error(error);
      }

      $('.kuvaboxi a.kuvaotsikko').each(async (_, kuvaboxi) => {
        const browseUrl = $(kuvaboxi).attr('href');
        const imageId = browseUrl ? utils.captureKuvatonIdFromUri(browseUrl) : undefined;

        if (!imageId || !browseUrl) {
          done();
          return;
        }

        if (options.onlyNew && shouldContinueFetching(options)) {
          const obj = await findImageById(imageId);

          if (obj) {
            existingImageIDsCrawled += 1;
          }

          individualImagePageCrawler.queue(browseUrl);
        } else if (shouldContinueFetching(options)) {
          individualImagePageCrawler.queue(browseUrl);
        }
      });

      $('a.arrowNext').each((_, element) => {
        const url = $(element).attr('href');

        if (url && shouldContinueFetching(options)) {
          cache.pageURIs.push(url);
          pageCrawler.queue(url);
        }
      });

      done();
    },
  });

  pageCrawler.queue(settings.entryUri);

  return new Promise((resolve) => {
    const interval = setInterval(() => {
      console.log('QUEUE', pageCrawler.queueSize, individualImagePageCrawler.queueSize);

      if (pageCrawler.queueSize === 0 && individualImagePageCrawler.queueSize === 0) {
        console.log('Done crawling.');
        clearInterval(interval);
        resolve(undefined);
      }
    }, 1000);
  });
}
