// https://stackoverflow.com/questions/69746672/unable-to-resolve-path-to-module-firebase-admin-app-eslint
/* eslint-disable import/no-unresolved */
import { initializeApp } from 'firebase-admin/app';
import { /* FieldPath,*/ getFirestore, QueryDocumentSnapshot } from 'firebase-admin/firestore';
import { onRequest, onCall } from 'firebase-functions/v2/https';
/* eslint-enable import/no-unresolved */

initializeApp();

import { startCrawling, Options as CrawlerOptions } from './crawler';

const firestoreDb = getFirestore();
const kuvatonCollection = firestoreDb.collection('kuvatondb');
// const documentIdField = FieldPath.documentId();

const docToKuvatonObj = (doc: QueryDocumentSnapshot) => ({
  id: doc.id,
  imageId: doc.get('imageId'),
  imageUrl: doc.get('imageUrl'),
  thumbnailUrl: doc.get('thumbnailUrl'),
  browserUrl: doc.get('browserUrl'),
  title: doc.get('title'),
  rating: doc.get('rating'),
  comments: doc.get('comments'),
});

export const getImages = onRequest(
  {
    region: 'europe-north1',
    cors: true,
    cpu: 1,
    concurrency: 1,
    maxInstances: 1,
  },
  async (req, res) => {
    const { limit: queryLimit, direction = 'next' } = req.query;

    const imageId =
      req.query.imageId && typeof req.query.imageId === 'string'
        ? parseInt(req.query.imageId)
        : undefined;

    if (imageId && !Number.isFinite(imageId)) {
      res.status(400).send('Invalid image id');
      return;
    }

    const queryLimitNumber =
      typeof queryLimit === 'string' && Number.isInteger(parseInt(queryLimit))
        ? parseInt(queryLimit)
        : undefined;

    const limitParam = queryLimitNumber && queryLimitNumber <= 100 ? queryLimitNumber : 50;

    const querySnapshot = imageId
      ? await kuvatonCollection
          .where('imageId', direction === 'previous' ? '>' : '<', imageId)
          .orderBy('imageId', 'desc')
          .limit(limitParam)
          .get()
      : await kuvatonCollection.limit(limitParam).orderBy('imageId', 'desc').get();

    res.json(querySnapshot.docs.map(docToKuvatonObj));
  },
);

export const crawlImages = onCall<CrawlerOptions>(
  {
    region: 'europe-north1',
    invoker: 'private',
  },
  async (req) => {
    await startCrawling(req.data);
  },
);

// Not used by the KuvatonBrowser
// export const getImageById = onRequest({ region: 'europe-north1' }, async (req, res) => {
//   const imageId = parseInt(req.params.imageId);

//   if (!Number.isFinite(imageId)) {
//     res.status(400).send('Invalid image id');
//     return;
//   }

//   const querySnapshot = await kuvatonCollection
//     .where('imageId', '==', imageId)
//     .orderBy(documentIdField)
//     .get();

//   if (querySnapshot.docs.length === 0) {
//     res.sendStatus(404);
//     return;
//   }

//   res.json(docToKuvatonObj(querySnapshot.docs[0]));
// });
