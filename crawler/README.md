# kuvatonCrawler

A simple crawler with Node.js to crawl image urls and meta information from
a popular image sharing web site to MongoDB. Includes also a server to provide
a simple API to fetch the data as JSON. Just a hobby project.

## Usage

Install dependencies by `npm install`.

Crawl one page of images by running `node crawler.js -p 1`.
Mongo must be running on the background.

### Test script locally

```sh
npm run build
```

```sh
npm node build/crawler.js -p 1
```

### Example cronjob

```cron
0 6,17,21 * * * node crawler.js --only-new 50
```

### Google Cloud usage

Deployed instance test url: https://kuvatonbrowser-api.wm.r.appspot.com

Install

Init & login

```sh
gcloud init
gcloud auth login
```

Setup

```sh
gcloud config set project kuvatonbrowser-api
```

Deploy

```sh
gcloud app deploy
```

Update deployment

```sh
gcloud app update
```

(inside `src/api`` directory)

Open in browser

```sh
gcloud app browse
```

Stream logs

```sh
gcloud app logs tail -s default
```

Links
* [App Engine](https://console.cloud.google.com/appengine)
* [app.yaml reference](https://cloud.google.com/appengine/docs/standard/reference/app-yaml?tab=node.js)
*