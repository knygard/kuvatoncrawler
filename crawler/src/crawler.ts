import fs from 'node:fs';
import path from 'node:path';
import url from 'node:url';
import stream from 'node:stream';
import Crawler from 'crawler';
import mongoose from 'mongoose';
import optimist from 'optimist';
import { mkdirp } from 'mkdirp';
import clc from 'cli-color';
import * as utils from './utils';
import firebaseAdmin from 'firebase-admin';

const serviceAccount = require('../kuvatonbrowser-firebase-adminsdk-849q5-262ab22dc3.json');

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
});

const firestoreDb = firebaseAdmin.firestore();
const kuvatonCollection = firestoreDb.collection('kuvatondb');
const documentIdField = firebaseAdmin.firestore.FieldPath.documentId();

const argv = optimist
  .usage('Usage: $0 -p [num] -d [dir]')
  .alias('d', 'debug')
  .alias('p', 'pages')
  .alias('i', 'images')
  .alias('s', 'save-dir')
  .alias('o', 'only-new')
  .describe('p', 'Max. amount of pages to crawl')
  .describe('i', 'Max. amount of images to crawl')
  .describe('d', 'Download images to this directory')
  .describe(
    'o',
    'Crawl images until an existing image is found and update' +
      'given approximate amount of images after that ' +
      '(defaults to 50).',
  ).argv;

const settings = {
  entryUri: 'http://kuvaton.com',
  timeout: 5, // seconds
};

if (!argv.debug) {
  mongoose.connect('mongodb://127.0.0.1:27017/kuvatonDb');
}

const cache: { pageURIs: string[]; imageURIs: string[] } = { pageURIs: [], imageURIs: [] };
let existingImageIDsCrawled = 0;

function shouldContinueFetching() {
  if (argv.images && cache.imageURIs.length >= parseInt(argv.images)) {
    return false;
  }

  if (argv.pages && cache.pageURIs.length >= parseInt(argv.pages)) {
    return false;
  }

  if (argv.o && existingImageIDsCrawled >= parseInt(argv.o || 50)) {
    return false;
  }

  return true;
}

const findImageById = async (imageId: number) => {
  const querySnapshot = await kuvatonCollection.where('imageId', '==', imageId).get();
  return querySnapshot.docs[0] ?? undefined;
};

const pageCrawler = new Crawler({
  maxConnections: 1,
  jQuery: true,
  callback: async (error, result, done) => {
    const { $ } = result;

    if (error) {
      done();
      return console.error(error);
    }

    $('.kuvaboxi a.kuvaotsikko').each(async (_, kuvaboxi) => {
      const browseUrl = $(kuvaboxi).attr('href');
      const imageId = browseUrl ? utils.captureKuvatonIdFromUri(browseUrl) : undefined;

      if (!imageId || !browseUrl) {
        done();
        return;
      }

      if (argv.o && shouldContinueFetching()) {
        const obj = await findImageById(imageId);

        if (obj) {
          existingImageIDsCrawled += 1;
        }

        individualImagePageCrawler.queue(browseUrl);
      } else if (shouldContinueFetching()) {
        individualImagePageCrawler.queue(browseUrl);
      }
    });

    $('a.arrowNext').each((_, element) => {
      const url = $(element).attr('href');

      if (url && shouldContinueFetching()) {
        cache.pageURIs.push(url);
        pageCrawler.queue(url);
      }
    });

    done();
  },
});

type ImageEntry = {
  imageId: number;
  imageUrl: string;
  thumbnailUrl: string;
  browseUrl: string;
  title: string;
  rating: number;
  comments: {
    date: Date;
    user: string;
    message: string;
  }[];
};

let timeout: ReturnType<typeof setTimeout>;

const individualImagePageCrawler = new Crawler({
  maxConnections: 10,
  jQuery: true,
  callback: async (error, result, done) => {
    const {
      $,
      options: { uri: browseUri = '' },
    } = result;

    if (error) {
      done();
      return console.error(error);
    }

    const imgSrc = $('#nayta_nykyinen img').attr('src');

    if (!imgSrc) {
      done();
      return;
    }

    const imageId = utils.captureKuvatonIdFromUri(browseUri);

    if (!imageId) {
      done();
      throw new Error(`imageId was not readable from uri: ${browseUri}`);
    }

    const imageEntry: ImageEntry = {
      imageId,
      imageUrl: url.parse(imgSrc).href,
      thumbnailUrl: url.parse(imgSrc.replace('/kuvei/', '/kuvei_thumb/')).href,
      browseUrl: url.parse(browseUri).href,
      title: $('#nayta_nykyinen .kuvanimi').text().trim() || path.basename(imgSrc),
      rating: parseInt($('#nayta_kuva .arvosana').text()),
      comments: $('#content .kuvakommentit table')
        .map((_, element) => {
          const comment = $(element);

          return {
            date: utils.parseKuvatonDate(comment.find('.aika').text()),
            user: utils.parseKuvatonUser(comment.find('.tunnus').text()),
            message: comment.find('.viesti').html() ?? '',
          };
        })
        .get(),
    };

    if (argv.debug) {
      console.log('Image entry: ', JSON.stringify(imageEntry, null, 2));
    }

    saveImageEntry(imageEntry);

    if (argv.ddir) {
      try {
        await mkdirp(argv.ddir);
        downloadImage(imgSrc, argv.ddir, path.basename(imgSrc));
      } catch (err) {
        done();
        console.log(clc.red('Could not create directory:', argv.ddir));
      }
    }

    // exit when there are no new images
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      console.log('Done.');
      process.exit();
    }, settings.timeout * 1000);

    cache.imageURIs.push(imgSrc);

    done();
  },
});

async function downloadImage(uri: string, toDir: string, filename: string) {
  const writePath = path.resolve(toDir, filename);

  const ws = fs.createWriteStream(writePath);

  ws.on('error', () => {
    console.error('Error writing image');
  });

  try {
    // https://stackoverflow.com/questions/44672942/stream-response-to-file-using-fetch-api-and-fs-createwritestream

    const res = await fetch(uri);

    if (!res.body) {
      return console.error('Response body was empty:', uri);
    }

    stream.Readable.fromWeb(res.body as any).pipe(ws);

    clc.cyan('DOWNLOAD', writePath);
  } catch (err) {
    console.error(clc.red('Error downloading image:', uri));
  }
}

async function saveImageEntry(entry: ImageEntry) {
  try {
    const image = await findImageById(entry.imageId);

    if (image?.ref) {
      image.ref.update(entry);
    } else {
      await kuvatonCollection.doc().set(entry);
    }

    if (image?.ref) {
      console.log(
        clc.yellow('UPDATE\t', entry.imageId, '(' + entry.comments.length + ')', entry.title),
      );
    } else {
      console.log(
        clc.green('NEW\t', entry.imageId, '(' + entry.comments.length + ')', entry.title),
      );
    }
  } catch (err) {
    console.log('Error in saving KuvatonEntry:\n', err);
  }
}

pageCrawler.queue(settings.entryUri);
