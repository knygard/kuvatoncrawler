export function parseKuvatonDate(dateString: string) {
  // kuvaton dates are written like 12.06.15 17:25

  const parts = dateString.split(' ');
  const firstParts = parts[0].split('.');
  const lastParts = parts[1].split(':');

  // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
  // Note: months are 0-based
  return new Date(
    // @ts-ignore
    '20' + firstParts[2],
    // @ts-ignore
    firstParts[1] - 1,
    // @ts-ignore
    firstParts[0] - 1,
    // @ts-ignore
    lastParts[0],
    // @ts-ignore
    lastParts[1],
  );
}

export function parseKuvatonUser(userString: string) {
  // kuvaton users are like <nickNameHere>

  return userString.substring(1, userString.length - 1);
}

export function captureKuvatonIdFromUri(uri: string): number | undefined {
  // kuvaton uris are like
  // http://kuvaton.com/browse/36341/pic.jpeg

  const re = /\/browse\/(\d{1,8})\//;
  const id = re.exec(uri)?.[1] ?? undefined;
  return id ? parseInt(id) : undefined;
}
